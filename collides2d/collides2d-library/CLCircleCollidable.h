/*
 * collides2d - simple collision detection extension for cocos2d for iPhone.
 *
 * Copyright (C) 2013 Scott Thiebes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


/** Framework imports **/
#import <Foundation/Foundation.h>
/** Project imports **/
#import "CLCollidable.h"
#import "CLProjection.h"


/**
 * CLCircleCollidable implements a circle shaped collideable with a specific radius.
 *
 * @author Scott Thi
 * @since v0.1
 * @date 12/12/2012
 */
@interface CLCircleCollidable : CLCollidable

#pragma mark - Properties
/** The radius of the collidable circle. */
@property (nonatomic, assign) float radius;


#pragma mark - Initialization
/** Initializes a new instance of this class with its radius. */
- (id)initWithRadius:(float)r;


#pragma mark - Methods
/** Projects the circle onto the passed axis. */
- (CLProjection*)project:(CGPoint)axis;
@end
