/*
 * collides2d - simple collision detection extension for cocos2d for iPhone.
 *
 * Copyright (C) 2013 Scott Thiebes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


/** Framework imports **/
#import "CLCollidable.h"
/** Project imports **/
// none


#pragma mark - Denug draw constants
const ccColor4F COLLIDABLE_EDGE_COLOR = {0.8, 1.0, 0.76, 1.0};
const ccColor4F COLLIDABLE_BOUNDING_BOX_COLOR = {0, 0, 1.0, 1.0};
const ccColor4F COLLIDABLE_COLLISION_COLOR = {1.0, 0, 0, 1.0};
const int CIRCLE_SHAPE_DEBUG_EDGE_COUNT = 32;


@implementation CLCollidable


#pragma mark - Initialization
- (id)initAsCircle:(BOOL)circle
{
    if((self = [super init])) {
        _isCircle = circle;
    }
    
    return self;
}


#pragma mark - Methods
- (CGRect)boundingBox
{
    NSAssert(NO, @"Subclasses need to override this method.");
    return CGRectMake(-1, -1, 1, 1);
}


- (void)draw
{
    
    if(self.drawDebug) {
     
        
        if(self.collides) {
            ccDrawColor4F(COLLIDABLE_COLLISION_COLOR.r, COLLIDABLE_COLLISION_COLOR.g,
                          COLLIDABLE_COLLISION_COLOR.b, COLLIDABLE_COLLISION_COLOR.a);
        } else {
            ccDrawColor4F(COLLIDABLE_BOUNDING_BOX_COLOR.r, COLLIDABLE_BOUNDING_BOX_COLOR.g,
                          COLLIDABLE_BOUNDING_BOX_COLOR.b, COLLIDABLE_BOUNDING_BOX_COLOR.a);
        }
        
        CGRect boundingBox = [self boundingBox];
        CGPoint p2 = ccp(boundingBox.origin.x + boundingBox.size.width,
                         boundingBox.origin.y + boundingBox.size.height);
        ccDrawRect(ccpSub(boundingBox.origin, self.position), ccpSub(p2, self.position));
    }
}


@end
