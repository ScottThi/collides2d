/*
 * collides2d - simple collision detection extension for cocos2d for iPhone.
 *
 * Copyright (C) 2013 Scott Thiebes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


/** Framework imports **/
#import <Foundation/Foundation.h>
/** Project imports **/
#import "CLCollidable.h"


/**
 *
 * @since v0.1
 * @date 01/01/2013
 */
@interface CLCollisionGroup : NSObject {
    
    NSMutableArray* collidables;
}


#pragma mark - Properties
@property (nonatomic, readonly) NSString* name;


#pragma mark - Initialization
- (id)initWithName:(NSString*)groupName;


#pragma mark - Public methods
- (CLCollidable*)getCollidable:(int)index;
- (int)count;
- (void)addCollidable:(CLCollidable*)collidable;
- (void)removeCollidable:(CLCollidable*)collidable;
- (BOOL)containsCollidable:(CLCollidable*)collidable;
@end
