/*
 * collides2d - simple collision detection extension for cocos2d for iPhone.
 *
 * Copyright (C) 2013 Scott Thiebes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


/** Framework imports **/
// none
/** Project imports **/
#import "CLPolygonCollidable.h"


@implementation CLPolygonCollidable


#pragma mark - Initialization
- (id)init
{
    if((self = [super initAsCircle:NO])) {
        vertices = [[NSMutableArray alloc] init];
        self.position = ccp(0, 0);
        self.rotation = 0;
        
        _winding = ClockWisePolygonWinding;
    }
    
    return self;
}

- (id)initWithVertex:(CGPoint)v
{
    if((self = [super initAsCircle:NO])) {
        vertices = [[NSMutableArray alloc] initWithObjects:[NSValue valueWithCGPoint:v], nil];
        self.position = ccp(0, 0);
        self.rotation = 0;
        
        _winding = ClockWisePolygonWinding;
    }
    
    return self;
}

 
- (id)initWithVertices:(NSArray *)v
{
    if((self = [super initAsCircle:NO])) {
        vertices = [[NSMutableArray alloc] initWithArray:v];
        self.position = ccp(0, 0);
        self.rotation = 0;
        
        _winding = ClockWisePolygonWinding;
    }
    
    return self;
}


- (id)initWithRectangle:(float)width andHeight:(int)height
{
    if((self = [super initAsCircle:NO])) {
        self.position = ccp(0, 0);
        self.rotation = 0;
        
        
        float halfWidth = width * 0.5;
        float halfHeight = width * 0.5;
        
        vertices = [[NSMutableArray alloc] initWithCapacity:4];
        // Create rectangle points.
        [vertices addObject:[NSValue valueWithCGPoint:ccp(-halfWidth, -halfHeight)]];
        [vertices addObject:[NSValue valueWithCGPoint:ccp(-halfWidth, halfHeight)]];
        [vertices addObject:[NSValue valueWithCGPoint:ccp(halfWidth, halfHeight)]];
        [vertices addObject:[NSValue valueWithCGPoint:ccp(halfWidth, -halfHeight)]];
        
        _winding = ClockWisePolygonWinding;
    }

    
    return self;
}


- (id)initWithVertex:(CGPoint)v andWinding:(CLPolygonWindingType)winding
{
    if((self = [self initWithVertex:v])) {
        _winding = winding;
    }
    
    return self;
}


- (id)initWithVertices:(NSArray *)v andWinding:(CLPolygonWindingType)winding
{
    if((self = [self initWithVertices:v])) {
        _winding = winding;
    }
    
    return self;
}


- (id)initWithRectangle:(float)width andHeight:(int)height andWinding:(CLPolygonWindingType)winding
{
    if((self = [self initWithRectangle:width andHeight:height])) {
        _winding = winding;
    }
    
    return self;
}



#pragma mark - Private methods
- (CGPoint)vertex:(int)i
{
    
    CGPoint p0 = [self convertToWorldSpace:CGPointZero];
    CGPoint p = [(NSValue*)[vertices objectAtIndex:i] CGPointValue];
    
    // Rotate vertex around center.
    if(self.rotation != 0) {
        p = ccpRotateByAngle(p, CGPointZero, self.rotation);
    }
    
    
    
    return ccpAdd(p, p0);
    
    /*
    // Return final vertex position. If node has been moved, add translation vector to node.
    if(self.position.x != 0 && self.position.y != 0) {
        return ccpAdd(p, self.position);
    } else {
        return p;
    }*/
}



#pragma mark - Methods
- (int)numberOfVertices
{
    return [vertices count];
}


- (BOOL)addVertex:(CGPoint)v
{
    // ToDo: Check if vertex allready exists for this polygon.
    
    [vertices addObject:[NSValue valueWithCGPoint:v]];
    return YES;
}


- (BOOL)removeVertex:(int)index
{
    if([vertices count] == 1 || index >= [vertices count]) {
        return NO;
    }
    
    
    [vertices removeObjectAtIndex:index];
    return YES;
}


- (int)numberOfEdges
{
    if([self numberOfVertices] <= 2) {
        return [self numberOfVertices] - 1;
    } else {
        return [self numberOfVertices];
    }
}


- (CGPoint)normal:(int)edgeVertex
{
    int nextEdgeVertex = (edgeVertex + 1) % [vertices count];
    CGPoint p0 = [self convertToWorldSpace:CGPointZero];
    CGPoint p1 = ccpSub([self vertex:edgeVertex], p0);
    CGPoint p2 = ccpSub([self vertex:nextEdgeVertex], p0);

    CGPoint edge = ccpSub(p2, p1);
    
    
    edge = ccpNormalize(edge);
    
    if(self.winding == ClockWisePolygonWinding) 
        return ccpRPerp(edge);
    else if(self.winding == CounterClockWisePolygonWinding)
        return ccpPerp(edge);
    else
        return CGPointZero;
}


- (CLProjection*)project:(CGPoint)axis
{
    float min = ccpDot(axis, [self vertex:0]);
    float max = min;
    
    for(int i = 1; i < [self numberOfVertices]; i++) {
        float p = ccpDot(axis, [self vertex:i]);
        
        if(p < min)
            min = p;
        else if(p > max)
            max = p;
    }

    
    return [[CLProjection alloc] initWithMin:min andMax:max];
}


- (CGPoint)closestVertexTo:(CGPoint)v
{
    // ---------------------------------------------------------------------------------------------
    // ToDo: Use Voronoi Regions for faster checks.
    // ---------------------------------------------------------------------------------------------
    
    
    CGPoint minVertex = [self vertex:0];
    float minDistance = ccpDistance(minVertex, v);
    for(int i = 1; i < [self numberOfVertices]; i++) {
        
        CGPoint nextVertex = [self vertex:i];
        float distance = ccpDistance(nextVertex, v);
        if(distance < minDistance) {
            minVertex = nextVertex;
            minDistance = distance;
        }
    }
    
    return minVertex;
}


- (CGRect)boundingBox
{
    CGPoint p0 = [self vertex:0];
    float minX = p0.x;
    float maxX = p0.x;
    float minY = p0.y;
    float maxY = p0.y;
    
    for(int i = 1; i < [vertices count]; i++) {
        CGPoint p = [self vertex:i];
        
        // Calculate min and max horizontal coordinates.
        if(p.x < minX) minX = p.x;
        else if(p.x > maxX) maxX = p.x;
        // Calculate min and max vertical coordinates.
        if(p.y < minY) minY = p.y;
        else if(p.y > maxY) maxY = p.y;
    }
    
    // Calculate horizontal and vertical distances.
    float distX = abs(maxX - minX);
    float distY = abs(maxY - minY);
    
    return CGRectMake(minX, minY, distX, distY);
}



- (void)draw
{
    // Draw bounding box.
    [super draw];
    
    
    if(!self.drawDebug)
        return;
    
   
    
    
    for(int i = 0; i < [vertices count]; i++) {
        
        if(self.collides) {
            ccDrawColor4F(COLLIDABLE_COLLISION_COLOR.r, COLLIDABLE_COLLISION_COLOR.g,
                          COLLIDABLE_COLLISION_COLOR.b, COLLIDABLE_COLLISION_COLOR.a);
        } else {
            ccDrawColor4F(COLLIDABLE_EDGE_COLOR.r, COLLIDABLE_EDGE_COLOR.g,
                          COLLIDABLE_EDGE_COLOR.b, COLLIDABLE_EDGE_COLOR.a);
        }
        
        int next = (i + 1) % [vertices count];
        
    
        CGPoint p0 = [self convertToWorldSpace:CGPointZero];
        CGPoint p1 = [self vertex:i];
        CGPoint p2 = [self vertex:next];
    
        
        ccDrawCircle(ccpSub(p1, p0), 4, 0, CIRCLE_SHAPE_DEBUG_EDGE_COUNT, NO);
        ccDrawLine(ccpSub(p1, p0), ccpSub(p2, p0));
    }
    
    
}

@end
