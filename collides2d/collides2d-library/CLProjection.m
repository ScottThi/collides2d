/*
 * collides2d - simple collision detection extension for cocos2d for iPhone.
 *
 * Copyright (C) 2013 Scott Thiebes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


/** Framework imports **/
// none
/** Project imports **/
#import "CLProjection.h"


@implementation CLProjection


#pragma mark - Initialization
- (id)initWithMin:(float)min andMax:(float)max
{
    if((self=[super init])) {
        
        _min = min;
        _max = max;
        
    }
    
    return self;
}


#pragma mark - Methods
- (BOOL)overlaps:(CLProjection *)proj
{
    //return (self.max > proj.min || self.min > proj.max);
    //!(this->min > other->max || other->min > this->max);
    return !(self.min > proj.max || proj.min > self.max);
}


- (float)getOverlap:(CLProjection *)proj
{
    // Return 0 if projections do not overlap.
    if(![self overlaps:proj]) {
        return 0;
    }
    
    // Calculate possible overlaps.
    float d1 = ABS(self.max - proj.min);
    float d2 = ABS(self.min - proj.max);
    
    // Return overlap based upon overlap type.
    if(self.max > proj.min || self.min > proj.max) {
        return (d1 < d2) ? d1 : d2;
    } else {
        CCLOG(@"Invalid overlap condition.");
    }
    
    return 0;
}

@end
