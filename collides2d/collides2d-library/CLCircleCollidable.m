/*
 * collides2d - simple collision detection extension for cocos2d for iPhone.
 *
 * Copyright (C) 2013 Scott Thiebes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


/** Framework imports **/
// none
/** Project imports **/
#import "CLCircleCollidable.h"


@implementation CLCircleCollidable


#pragma mark - Initialization
- (id)initWithRadius:(float)r
{
    if((self = [super initAsCircle:YES])) {
        _radius = r;
        self.position = ccp(0, 0);
        self.rotation = 0;
    }
    
    return self;
}


#pragma mark - Methods
- (CGRect)boundingBox
{
    CGPoint p0 = [self convertToWorldSpace:CGPointZero];
    
    
    
    /*return CGRectMake(self.position.x - self.radius, self.position.y - self.radius,
                      self.radius * 2, self.radius * 2);*/
    
    return CGRectMake(p0.x - self.radius, p0.y - self.radius, self.radius * 2, self.radius * 2);
}


- (CLProjection*)project:(CGPoint)axis
{
    
    float p = ccpDot(axis, self.position);
    
    float min = p - self.radius;
    float max = p + self.radius;
    
    
    return [[CLProjection alloc] initWithMin:min andMax:max];
}



- (void)draw
{
    [super draw];
    
    if(!self.drawDebug)
        return;
    
    if(self.collides) {
        ccDrawColor4F(COLLIDABLE_COLLISION_COLOR.r, COLLIDABLE_COLLISION_COLOR.g,
                      COLLIDABLE_COLLISION_COLOR.b, COLLIDABLE_COLLISION_COLOR.a);
    } else {
        ccDrawColor4F(COLLIDABLE_EDGE_COLOR.r, COLLIDABLE_EDGE_COLOR.g,
                      COLLIDABLE_EDGE_COLOR.b, COLLIDABLE_EDGE_COLOR.a);
    }
    
    ccDrawCircle(CGPointZero, self.radius, self.rotation, CIRCLE_SHAPE_DEBUG_EDGE_COUNT, YES);
}
@end
