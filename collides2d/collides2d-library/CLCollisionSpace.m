/*
 * collides2d - simple collision detection extension for cocos2d for iPhone.
 *
 * Copyright (C) 2013 Scott Thiebes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


/** Framework imports **/
// none
/** Project imports **/
#import "CLCollisionSpace.h"
#import "CLCollidable.h"
#import "CLCircleCollidable.h"
#import "CLPolygonCollidable.h"


#pragma mark - Private Declarations
@interface CLCollisionSpace(Private)
- (BOOL)checkCollisionOfA:(CLCollidable*)a withB:(CLCollidable*)b;
- (BOOL)checkBoundingBoxCollisionOfA:(CLCollidable*)a withB:(CLCollidable*)b;
- (BOOL)circle:(CLCircleCollidable*)a collidesWithCircle:(CLCircleCollidable*)b;
- (BOOL)polygon:(CLPolygonCollidable*)a collidesWithCircle:(CLCircleCollidable*)b;
- (BOOL)polygon:(CLPolygonCollidable*)a collidesWithPolygon:(CLPolygonCollidable*)b;
@end


@implementation CLCollisionSpace


#pragma mark - Initialization
- (id)init
{
    if((self = [super init])) {
        collidables = [[NSMutableArray alloc] initWithCapacity:10];
        collisions = [[NSMutableArray alloc] init];
        
        // Create dictionary to hold collision groups and main collision group ('WorldSpace').
        collisionGroups = [[NSMutableDictionary alloc] initWithCapacity:1];
        worldSpace = [[CLCollisionGroup alloc] initWithName:@"WorldSpace"];
        [collisionGroups setObject:worldSpace forKey:worldSpace.name];
    }
    
    return self;
}


#pragma mark - Private methods
- (BOOL)checkCollisionOfA:(CLCollidable *)a withB:(CLCollidable *)b
{
    if([self checkBoundingBoxCollisionOfA:a withB:b]) {
        
        // Circle vs circle collision detection.
        if(a.isCircle && b.isCircle) {
            
            CLCircleCollidable* cA = (CLCircleCollidable*)a;
            CLCircleCollidable* cB = (CLCircleCollidable*)b;
            return [self circle:cA collidesWithCircle:cB];
            
        // Circle vs polygon collision detection.
        } else if(a.isCircle && !b.isCircle) {
            
            CLCircleCollidable* cA = (CLCircleCollidable*)a;
            CLPolygonCollidable* pB = (CLPolygonCollidable*)b;
            return [self polygon:pB collidesWithCircle:cA];
        
        // Polygon vs circle collision detection (the same as circle vs polygon).
        } else if(!a.isCircle && b.isCircle) {
            
            CLPolygonCollidable* pA = (CLPolygonCollidable*)a;
            CLCircleCollidable* cB = (CLCircleCollidable*)b;
            return [self polygon:pA collidesWithCircle:cB];
            
        // Polygon vs polygon collision detection.
        } else {
            
            CLPolygonCollidable* pA = (CLPolygonCollidable*)a;
            CLPolygonCollidable* pB = (CLPolygonCollidable*)b;
            return [self polygon:pA collidesWithPolygon:pB];
        }
    }
    
    return NO;
}


- (BOOL)checkBoundingBoxCollisionOfA:(CLCollidable *)a withB:(CLCollidable *)b
{
    return CGRectIntersectsRect(a.boundingBox, b.boundingBox);
}


- (BOOL)circle:(CLCircleCollidable *)a collidesWithCircle:(CLCircleCollidable *)b
{
    float dx = a.position.x - b.position.x;
    float dy = a.position.y - b.position.y;
    float radii = a.radius + b.radius;
    
    // Compare distance to combined radii.
    BOOL collides = ((dx*dx) + (dy*dy) < radii*radii);
    
    if(collides) {
        
        CGPoint axis = ccpSub(b.position, a.position);          // Minimum overlap for A and B.
        float overlap = (radii - ccpLength(axis));              // Movement vector for minimum overlap of A and B
        axis = ccpNormalize(axis);
        
        
        CLMinimumTranslationVector* mtv = [[CLMinimumTranslationVector alloc]
                                         initWithAxis:axis andOverlap:overlap];
        CLCollision* collision = [[CLCollision alloc] initWithA:a andB:b andMTV:mtv];
        [collisions addObject:collision];
    }
    
    return collides;
}


- (BOOL)polygon:(CLPolygonCollidable *)a collidesWithCircle:(CLCircleCollidable *)b
{
    // ---------------------------------------------------------------------------------------------
    // ToDo: May be still somewhat buggy in case bounding box collision detection is disabled.
    // ---------------------------------------------------------------------------------------------
    
    
    float overlap = MAXFLOAT;       // Minimum overlap for A and B.
    CGPoint minAxis;                // Movement vector for minimum overlap of A and B.
    
    // Check edges of a.
    for(int i = 0; i < [a numberOfEdges]; i++) {
        
        CGPoint axis = [a normal:i];
        CLProjection* p1 = [a project:axis];
        CLProjection* p2 = [b project:axis];
        
        if(![p1 overlaps:p2]) {
            return NO;
        } else {
            float o = [p1 getOverlap:p2];
            
            if(o < overlap) {
                overlap = o;
                minAxis = axis;
            }
        }
    }
    
    // Get circle center and closest polygon vertex to circle center.
    CGPoint p1 = b.position;
    CGPoint p2 = [a closestVertexTo:b.position];
    
    // Create and normalize projection axis.
    CGPoint axis = ccpSub(p2, p1);
    axis = ccpPerp(axis);
    axis = ccpNormalize(axis);
    
    // Project polygon and circle onto axis.
    CLProjection* proj1 = [a project:axis];
    CLProjection* proj2 = [b project:axis];
    
    // If the projections overlap the two shapes do intersect,
    // otherwise they don't.
    if(![proj1 overlaps:proj2]) {
        return NO;
    } else {
        float o = [proj1 getOverlap:proj2];
        
        if(o < overlap) {
            overlap = o;
            minAxis = axis;
        }
    }
    
    
    // Create minimum translation vector and collision object and add collision object to list
    // of found collision.
    CLMinimumTranslationVector* mtv = [[CLMinimumTranslationVector alloc]
                                     initWithAxis:minAxis andOverlap:overlap];
    CLCollision* collision = [[CLCollision alloc] initWithA:a andB:b andMTV:mtv];
    [collisions addObject:collision];
    
    return YES;
}


- (BOOL)polygon:(CLPolygonCollidable *)a collidesWithPolygon:(CLPolygonCollidable *)b
{
    float overlap = MAXFLOAT;       // Minimum overlap for A and B.
    CGPoint minAxis;                // Movement vector for minimum overlap of A and B.
    
    
    // Check edges of A.
    for(int i = 0; i < [a numberOfEdges]; i++) {
        
        CGPoint axis = [a normal:i];
        CLProjection* p1 = [a project:axis];
        CLProjection* p2 = [b project:axis];
        
        if(![p1 overlaps:p2]) {
            return NO;
        } else {
            float o = [p1 getOverlap:p2];
            
            if(o < overlap) {
                overlap = o;
                minAxis = axis;
            }
        }
    }
    
    // Check edges of B.
    for(int i = 0; i < [b numberOfEdges]; i++) {
        CGPoint axis = [b normal:i];
        CLProjection* p1 = [a project:axis];
        CLProjection* p2 = [b project:axis];
        
        if(![p1 overlaps:p2]) {
            return NO;
        } else {
            float o = [p1 getOverlap:p2];
            
            if(o < overlap) {
                overlap = o;
                minAxis = axis;
            }
        }
    }
    

    // Create minimum translation vector and collision object and add collision object to list
    // of found collision.
    CLMinimumTranslationVector* mtv = [[CLMinimumTranslationVector alloc]
                                     initWithAxis:minAxis andOverlap:overlap];
    CLCollision* collision = [[CLCollision alloc] initWithA:a andB:b andMTV:mtv];
    [collisions addObject:collision];
    
    // Polygons A and B collide.
    return YES;
}



#pragma mark - Methods
- (int)numberOfCollidables
{
    int count = 0;
    
    for (NSString* key in collisionGroups) {
        CLCollisionGroup* group = (CLCollisionGroup*)[collisionGroups objectForKey:key];
        count += [group count];
    }
    
    return count;
}


- (void)addCollisionGroup:(CLCollisionGroup *)group
{
    if(![[collisionGroups allKeys] containsObject:group.name]) {
        [collisionGroups setObject:group forKey:group.name];
    }
}


- (CLCollisionGroup*)addCollisionGroupForName:(NSString *)name
{
    CLCollisionGroup* group = nil;
    
    if(![[collisionGroups allKeys] containsObject:name]) {
        group = [[CLCollisionGroup alloc] initWithName:name];
        [collisionGroups setObject:group forKey:name];
    }
    
    return group;
}


- (void)removeCollisionGroup:(CLCollisionGroup *)group
{
    if([[collisionGroups allKeys] containsObject:group.name] &&
       [group.name compare:worldSpace.name] != NSOrderedSame) {
        // Remove collision group.
        [collisionGroups removeObjectForKey:group.name];
    }
}


- (CLCollisionGroup*)getGroup:(NSString *)name
{
    CLCollisionGroup* group = nil;
    
    if([[collisionGroups allKeys] containsObject:name]) {
        group = [collisionGroups objectForKey:name];
    }
    
    return group;
}


- (BOOL)spaceContainsCollidable:(CLCollidable *)a
{
    
    return [worldSpace containsCollidable:a];
}


- (void)addCollidable:(CLCollidable *)a
{
    [worldSpace addCollidable:a];
}


-(void)addCollidable:(CLCollidable *)a toGroup:(NSString *)name
{
    CLCollisionGroup* group = [self getGroup:name];
    
    if(group == nil) {
        group = [self addCollisionGroupForName:name];
    }
    
    [group addCollidable:a];
}


- (void)removeCollidable:(CLCollidable *)a
{
    [worldSpace removeCollidable:a];
}


- (void)removeCollidable:(CLCollidable *)a fromGroup:(NSString *)name
{
    CLCollisionGroup* group = [self getGroup:name];
    
    if(group != nil) {
        [group removeCollidable:a];
    }
}


- (int)numberOfCollisions
{
    return [collisions count];
}


- (CLCollision*)getCollision:(int)i
{
    if(i < 0 || i >= [collisions count]) {
        return nil;
    }
    
    return (CLCollision*)[collisions objectAtIndex:i];
}


- (void)update
{
    // Reset all colliding objects to non-colliding.
    for(CLCollision* collision in collisions) {
        collision.collidableA.collides = NO;
        collision.collidableB.collides = NO;
    }
    
    // Remove all collision objects.
    [collisions removeAllObjects];
    
    // Cheack each pair of collidables in this space for collision.
    for (NSString* key in collisionGroups) {
        CLCollisionGroup* group = (CLCollisionGroup*)[collisionGroups objectForKey:key];
        int groupCount = [group count];
        
        for (int i = 0; i < groupCount; i++) {
            for(int j = i + 1; j < groupCount; j++) {
                // Get pair.
                CLCollidable* a = [group getCollidable:i];
                CLCollidable* b = [group getCollidable:j];
                // Check for collision.
                [self checkCollisionOfA:a withB:b];
            }
        }
    }
}


@end
