/*
 * collides2d - simple collision detection extension for cocos2d for iPhone.
 *
 * Copyright (C) 2013 Scott Thiebes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


/** Framework imports **/
#import <Foundation/Foundation.h>
#import "cocos2d.h"
/** Project imports **/
// none


/** Constants **/
#ifndef COLLIDABLE_H
#define COLLIDABLE_H
extern const ccColor4F COLLIDABLE_EDGE_COLOR;
extern const ccColor4F COLLIDABLE_BOUNDING_BOX_COLOR;
extern const ccColor4F COLLIDABLE_COLLISION_COLOR;
extern const int CIRCLE_SHAPE_DEBUG_EDGE_COUNT;
#endif


/**
 * CLCollidable implements the base class for all collidable objects/shapes.
 *
 * @author Scott Thi
 * @since v0.1
 * @date 12/14/2012
 */
@interface CLCollidable : CCNode

#pragma mark - Properties
/** The collidables position. */
@property (nonatomic, assign) CGPoint position;

/** The collidables rotation in degrees. */
@property (nonatomic, assign) float rotation;

/** Holds whether or not to draw this collidable. */
@property (nonatomic, assign) BOOL drawDebug;

/** Holds whether or not this collidable collides with another. */
@property (nonatomic, assign) BOOL collides;

/** Holds whether or not this collidable is a circle. */
@property (nonatomic, readonly) BOOL isCircle;


#pragma mark - Initialization
/** Initializes a new instance of class with a flag indicating whether this is a circle. */
- (id)initAsCircle:(BOOL)circle;


#pragma mark - Methods
/** Returns the bounding box for this collidable. 
 * @warning This method must be overriden by subclasses. Otherwise an assertion exception is thrown.
 */
- (CGRect)boundingBox;
@end
