/*
 * collides2d - simple collision detection extension for cocos2d for iPhone.
 *
 * Copyright (C) 2013 Scott Thiebes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

//
//  CollisionSpace.h
//  CollisionTest
//
//  Created by Scott on 12/20/2012.
//
//

/** Framework imports **/
#import <Foundation/Foundation.h>
#import "cocos2d.h"
/** Project imports **/
#import "CLCollision.h"
#import "CLCollisionGroup.h"


/**
 *
 * @since v0.1
 * @date 12/20/2012
 */
@interface CLCollisionSpace : NSObject {
    
    NSMutableArray* collidables;
    NSMutableArray* collisions;
    
    NSMutableDictionary* collisionGroups;
    CLCollisionGroup* worldSpace;
}

#pragma mark - Methods
- (int)numberOfCollidables;
- (void)addCollisionGroup:(CLCollisionGroup*)group;
- (CLCollisionGroup*)addCollisionGroupForName:(NSString*)name;
- (void)removeCollisionGroup:(CLCollisionGroup*)group;
- (CLCollisionGroup*)getGroup:(NSString*)name;
- (BOOL)spaceContainsCollidable:(CLCollidable*)a;
- (void)addCollidable:(CLCollidable*)a;
- (void)addCollidable:(CLCollidable*)a toGroup:(NSString*)name;
- (void)removeCollidable:(CLCollidable*)a;
- (void)removeCollidable:(CLCollidable *)a fromGroup:(NSString*)name;
- (int)numberOfCollisions;
- (CLCollision*)getCollision:(int)i;

- (void)update;

@end
