/*
 * collides2d - simple collision detection extension for cocos2d for iPhone.
 *
 * Copyright (C) 2013 Scott Thiebes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


/** Framework imports **/
#import <Foundation/Foundation.h>
/** Project imports **/
#import "CLCollidable.h"
#import "CLProjection.h"
#import "CLPolygonWindingType.h"


/**
 * CLPolygonCollidable implements a polygon shaped collidable for convex polygons.
 *
 * @author Scott Thi
 * @since v0.1
 * @date 12/14/2012
 */
@interface CLPolygonCollidable : CLCollidable {
    NSMutableArray* vertices;       /* Array of vertices for this polygon. */
}


#pragma mark - Properties
/** The polygons winding order of input vertices. */
@property (nonatomic, assign) CLPolygonWindingType winding;


#pragma mark - Initialization
/** Initializes a new instance of this class with its first vertex. */
- (id)initWithVertex:(CGPoint)v;

/** Initializes a new instance of this class with an array of vertices. */
- (id)initWithVertices:(NSArray*)v;

/** Initializes a new instance of this class as a rectangle with the given width and height. */
- (id)initWithRectangle:(float)width andHeight:(int)height;

/** Initializes a new instance of this class with its first vertex and winding order. */
- (id)initWithVertex:(CGPoint)v andWinding:(CLPolygonWindingType)winding;

/** Initializes a new instance of this class with an array of vertices and winding order. */
- (id)initWithVertices:(NSArray*)v andWinding:(CLPolygonWindingType)winding;

/** Initializes a new instance of this class as a rectangle with the given width and height and
  winding order. */
- (id)initWithRectangle:(float)width andHeight:(int)height andWinding:(CLPolygonWindingType)winding;


#pragma mark - Methods
/** Returns the current number of vertices for this polygon. */
- (int)numberOfVertices;

/** Returns the vertex at index i. */
- (CGPoint)vertex:(int)i;

/** Adds another vertex to this polygon. */
- (BOOL)addVertex:(CGPoint)v;

/** Removes a vertex at index i from this polygon. */
- (BOOL)removeVertex:(int)index;

/** Returns the number of edges for this vertex. */
- (int)numberOfEdges;

/** Calculates and returns the normal for the polygon edge at the given index. */
- (CGPoint)normal:(int)edgeVertex;

/** Projects this polygon onto the passed axis and returns the projection. */
- (CLProjection*)project:(CGPoint)axis;

/** Returns the polygon vertex closest to the given point. */
- (CGPoint)closestVertexTo:(CGPoint)v;
@end
