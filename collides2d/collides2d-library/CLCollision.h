/*
 * collides2d - simple collision detection extension for cocos2d for iPhone.
 *
 * Copyright (C) 2013 Scott Thiebes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


/** Framework imports **/
#import <Foundation/Foundation.h>
#import "cocos2d.h"
/** Project imports **/
#import "CLCollidable.h"
#import "CLMinimumTranslationVector.h"


/**
 * CLCollision implements a simple data structure to store collisions of two collidables and their
 * minimum translation vector.
 *
 * @author Scott Thi
 * @since v0.1
 * @date 12/20/2012
 */
@interface CLCollision : NSObject

#pragma mark - Properties
/** */
@property (unsafe_unretained, nonatomic, readonly) CLCollidable* collidableA;

/** */
@property (unsafe_unretained, nonatomic, readonly) CLCollidable* collidableB;

/** */
@property (unsafe_unretained, nonatomic, readonly) CLMinimumTranslationVector* mtv;


#pragma mark - Initialization
/** */
- (id)initWithA:(CLCollidable*)a andB:(CLCollidable*)b;

/** */
- (id)initWithA:(CLCollidable *)a andB:(CLCollidable *)b andMTV:(CLMinimumTranslationVector*)mtv;


#pragma mark - Initialization
/** */
- (BOOL)equals:(CLCollision*)other;
@end
