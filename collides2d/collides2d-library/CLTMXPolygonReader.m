/*
 * collides2d - simple collision detection extension for cocos2d for iPhone.
 *
 * Copyright (C) 2013 Scott Thiebes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


/** Framework imports **/
// none
/** Project imports **/
#import "CLTMXPolygonReader.h"
#import "CLPolygonCollidable.h"


@implementation CLTMXPolygonReader

+ (NSArray*)polygonsFromObjectLayer:(CCTMXObjectGroup *)objectLayer
{
    // If an empty object layer was returned, return nothing.
    if(objectLayer == nil) {
        return nil;
    }
    
    // Get polygon objects from object group and init array to hold actual polygons.
    NSMutableArray* polygonObjectArray = [objectLayer objects];
    NSMutableArray* polygons = [[NSMutableArray alloc] initWithCapacity:polygonObjectArray.count];
    // Define character set to split vertex coordinates.
    NSCharacterSet* characterSet = [NSCharacterSet characterSetWithCharactersInString:@", "];

    NSString* strPoints;    // String holding the vertices coordinates.
    NSArray* arrPoints;     // Array holding each vertex coordinate.
    int n = 0;              // Number of vertices for this polygon.
    float x, y, fX, fY;     // polygon position and relative position of current vertex.
    float area;             // Area used to calculate whether or not the polygon has CW or CCW winding.
    
    // Iterate over each polygon object found in the passed object group.
    for(id object in polygonObjectArray) {
        
        // Get string containung current polygons vertices information.
        strPoints = [object valueForKey:@"polygonPoints"];
        
        if(strPoints != NULL) {
            // Get array of integers representing the coordinates components.
            arrPoints = [strPoints componentsSeparatedByCharactersInSet:characterSet];
            n = arrPoints.count;
            
            // Initialize polygon with (0, 0)-Vertex because in the tiled map file format
            // the polygons first vertex always lies on the (0, 0)-Point.
            CLPolygonCollidable* polygon = [[CLPolygonCollidable alloc] init];
            // Get polygons position and place it there.
            x = [[object valueForKey:@"x"] intValue] / CC_CONTENT_SCALE_FACTOR();
            y = [[object valueForKey:@"y"] intValue] / CC_CONTENT_SCALE_FACTOR();
            polygon.position = ccp(x, y);
            
            // Iterate over all coordinates and add new vertex for each int pair.
            for(int i = 0; i < n; i+=2) {
                fX = [[arrPoints objectAtIndex:i] intValue] / CC_CONTENT_SCALE_FACTOR();
                fY = -[[arrPoints objectAtIndex:i+1] intValue] / CC_CONTENT_SCALE_FACTOR();
                
                [polygon addVertex:ccp(fX, fY)];
            }
            
            // Calculate area to see if polygon has CW or CCW winding.
            area = 0;
            for(int i = 0; i < [polygon numberOfVertices]; i++) {
                
                int j = (i + 1) % [polygon numberOfVertices];
                CGPoint p1 = [polygon vertex:i];
                CGPoint p2 = [polygon vertex:j];
                
                area += (p2.x * p1.y) - (p1.x * p2.y);
            }
            
            // For negative areas the polygon has a CCW winding, so change its winding type.
            if(area < 0) {
                polygon.winding = CounterClockWisePolygonWinding;
            }
            
            // Add polygon to array.
            [polygons addObject:polygon];
        }
    }
    
    // Return polygons.
    return polygons;
}
@end
