//
//  TiledPolygonsLayer.m
//  collides2d
//
//  Created by Scott on 03.01.13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "TiledPolygonsLayer.h"
#import "CLTMXPolygonReader.h"


@implementation TiledPolygonsLayer

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	TiledPolygonsLayer *layer = [TiledPolygonsLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}


#pragma mark - Initialization
- (id)init
{
    if((self = [super init])) {
        
        // Create collision space.
        cs = [[CLCollisionSpace alloc] init];
        
        // Create first small circle and add it to this scene and the collision space.
        c1 = [[CLCircleCollidable alloc] initWithRadius:25];
        [self addChild:c1];
        [cs addCollidable:c1];
        // Position circle outside visible area.
        c1.position = ccp(-100, -100);
        // Enable debug drawing of circle to see something.
        c1.drawDebug = YES;
        
        // Get polygons from tiled map object group.
        CCTMXTiledMap* map = [CCTMXTiledMap tiledMapWithTMXFile:@"Test01.tmx"];
        CCTMXObjectGroup* polygonsGroup = [map objectGroupNamed:@"pols"];
        NSArray* polygons = [CLTMXPolygonReader polygonsFromObjectLayer:polygonsGroup];
        
        // Add obtained polygons to collision space.
        for(CLPolygonCollidable* poly in polygons) {
            poly.drawDebug = YES;
            [self addChild:poly];
            [cs addCollidable:poly];
        }
        
                
        // Run update loop and enbale touches.
        [self schedule:@selector(update:)];
        self.isTouchEnabled = YES;
    }
    
    return self;
}


#pragma mark - Public methods
-(void)update:(ccTime)dt
{
    // Detect collisions in space.
    [cs update];
    
    
    // Iterate over all found collisions.
    for(int i = 0; i < [cs numberOfCollisions]; i++) {
        CLCollision* collision = [cs getCollision:i];
        // Resolve collision...
    }
}


- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    for (UITouch *t in touches) {
        CGPoint touchLocation = [self convertTouchToNodeSpace:t];
        
        // Position first polygon at touch location.
        c1.position = touchLocation;
    }
}


- (void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    for (UITouch *t in touches) {
        CGPoint touchLocation = [self convertTouchToNodeSpace:t];
        
        // Position first polygon at touch location.
        c1.position = touchLocation;
    }
}
@end
