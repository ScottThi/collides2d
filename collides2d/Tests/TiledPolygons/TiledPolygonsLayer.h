//
//  TiledPolygonsLayer.h
//  collides2d
//
//  Created by Scott on 03.01.13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

#import "CLCollidable.h"
#import "CLPolygonCollidable.h"
#import "CLCircleCollidable.h"
#import "CLCollisionSpace.h"

@interface TiledPolygonsLayer : CCLayer {
 
    // Takes collidable objects and checks for collisions.
    CLCollisionSpace* cs;
    
    // Movable collidable object.
    CLCircleCollidable* c1;
    
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;
@end
