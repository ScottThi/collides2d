//
//  CollisionGroups.h
//  collides2d
//
//  Created by Scott on 03.01.13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

#import "CLCollidable.h"
#import "CLPolygonCollidable.h"
#import "CLCircleCollidable.h"
#import "CLCollisionSpace.h"


@interface CollisionGroupsLayer : CCLayer {
 
    // Takes collidable objects and checks for collisions.
    CLCollisionSpace* cs;
    
    // Three simple collidable objects.
    CLCircleCollidable* c1;
    CLPolygonCollidable* c2;
    CLCircleCollidable* c3;    
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;
@end
