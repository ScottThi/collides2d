/*
 * collides2d - simple collision detection extension for cocos2d for iPhone.
 *
 * Copyright (C) 2013 Scott Thiebes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#import "BasicLayer.h"


@implementation BasicLayer

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	BasicLayer *layer = [BasicLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}


#pragma mark - Initialization
- (id)init
{
    if((self = [super init])) {
        
        // Create collision space.
        cs = [[CLCollisionSpace alloc] init];
        
        // Create first small rectangle polygon and add it to this scene and the collision space.
        c1 = [[CLPolygonCollidable alloc] initWithRectangle:50 andHeight:50];
        [self addChild:c1];
        [cs addCollidable:c1];
        // Position polygon outside visible area.
        c1.position = ccp(-100, -100);
        // Enable debug drawing of polygon to see something.
        c1.drawDebug = YES;
        
        
        // Create second, bigger rectangle polygon and add it to this scene and the collision space.
        c2 = [[CLPolygonCollidable alloc] initWithRectangle:100 andHeight:100];
        [self addChild:c2];
        [cs addCollidable:c2];
        // Position polygon at the center of the screen.
        c2.position = ccp(240, 160);
        // Enable debug drawing of polygon to see something.
        c2.drawDebug = YES;
        
        
        // Run update loop and enbale touches.
        [self schedule:@selector(update:)];
        self.isTouchEnabled = YES;
    }
    
    return self;
}



#pragma mark - Public methods
-(void)update:(ccTime)dt
{
    // Detect collisions in space.
    [cs update];
    
   
    // Iterate over all found collisions.
    for(int i = 0; i < [cs numberOfCollisions]; i++) {
        CLCollision* collision = [cs getCollision:i];
        // Resolve collision...
    }
}


- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    for (UITouch *t in touches) {
        CGPoint touchLocation = [self convertTouchToNodeSpace:t];
        
        // Position first polygon at touch location.
        c1.position = touchLocation;
    }
}


- (void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    for (UITouch *t in touches) {
        CGPoint touchLocation = [self convertTouchToNodeSpace:t];
        
        // Position first polygon at touch location.
        c1.position = touchLocation;
    }
}

@end
