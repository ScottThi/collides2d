//
//  ComplexObjectsLayer.m
//  collides2d
//
//  Created by Scott on 03.01.13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "ComplexObjectsLayer.h"


@implementation ComplexObjectsLayer

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	ComplexObjectsLayer *layer = [ComplexObjectsLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}


#pragma mark - Initialization
- (id)init
{
    if((self = [super init])) {
        
        // Create collision space.
        cs = [[CLCollisionSpace alloc] init];
        
        // Create first small circle and add it to this scene and the collision space.
        c1 = [[CLCircleCollidable alloc] initWithRadius:25];
        [self addChild:c1];
        [cs addCollidable:c1];
        // Position circle outside visible area.
        c1.position = ccp(-100, -100);
        // Enable debug drawing of circle to see something.
        c1.drawDebug = YES;
        
        
        // Create second polygon and add some vertices to it.
        c2 = [[CLPolygonCollidable alloc] initWithVertex:ccp(-50, -50)];
        // Add some vertices to the polygon.
        [c2 addVertex:ccp(-40, 15)];
        [c2 addVertex:ccp(50, 20)];
        [c2 addVertex:ccp(60, 0)];
        [c2 addVertex:ccp(20, -60)];
        // The above vertices were added in CW order. By default the winding is set to CW, so this
        // line is more or less useless. It's necessary only when using a CCW winding.
        [c2 setWinding:ClockWisePolygonWinding];
        [self addChild:c2];
        [cs addCollidable:c2];
        // Position polygon at the center of the screen.
        c2.position = ccp(240, 160);
        // Enable debug drawing of polygon to see something.
        c2.drawDebug = YES;
        
        // Create another, bigger circle and add it to this scene and the collision space.
        c3 = [[CLCircleCollidable alloc] initWithRadius:40];
        [self addChild:c3];
        [cs addCollidable:c3];
        // Position circle outside visible area.
        c3.position = ccp(60, 250);
        // Enable debug drawing of circle to see something.
        c3.drawDebug = YES;
        
        // Run update loop and enbale touches.
        [self schedule:@selector(update:)];
        self.isTouchEnabled = YES;
    }
    
    return self;
}



#pragma mark - Public methods
-(void)update:(ccTime)dt
{
    // Detect collisions in space.
    [cs update];
    
    
    // Iterate over all found collisions.
    for(int i = 0; i < [cs numberOfCollisions]; i++) {
        CLCollision* collision = [cs getCollision:i];
        // Resolve collision...
    }
}


- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    for (UITouch *t in touches) {
        CGPoint touchLocation = [self convertTouchToNodeSpace:t];
        
        // Position first polygon at touch location.
        c1.position = touchLocation;
    }
}


- (void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    for (UITouch *t in touches) {
        CGPoint touchLocation = [self convertTouchToNodeSpace:t];
        
        // Position first polygon at touch location.
        c1.position = touchLocation;
    }
}


@end
